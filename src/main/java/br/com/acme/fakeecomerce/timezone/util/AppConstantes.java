package br.com.acme.fakeecomerce.timezone.util;

public class AppConstantes {
    
	public static final String ABSOLUT_PATH = System.getProperty("user.dir");
	public static final String SEPARADOR = System.getProperty("file.separator");
	public static final String PATH_IMAGES = System.getProperty("user.dir") + SEPARADOR + "src" + SEPARADOR
			+ "resources" + SEPARADOR + "static" + SEPARADOR + "assets" + SEPARADOR + "img" + SEPARADOR;
	
	public static final String ITEM_CART =  "cartQuantidade";
	public static final String CHECKOUT =  "checkout";
}
